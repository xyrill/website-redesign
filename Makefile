all: draft/res/c3d2.css

draft/res/c3d2.css: draft/res/main.scss draft/res/*.scss draft/res/*/*.scss
	sassc -t compressed -I draft/res $< $@
