# Website-Redesign

In diesem Repo sammle ich Gedanken und Entwürfe für ein Redesign der [C3D2-Webseite](https://c3d2.de/).

## Howto

^^^
$ make
$ ( cd draft && python -m http.server 8080 )
^^^

-> http://localhost:8080

## Hierarchie aktuell

* Startseite: Kurzkalender und Kacheln
* Menüspalte 1
  * News
    * Seiten pro Veranstaltung
  * Über uns
  * Chaos macht Schule
* Menüspalte 2
  * Anfahrt
  * Mitgliedschaft
  * Unterstützen
* Menüspalte 3
  * Kalender
    * Seiten pro Veranstaltung
  * Themenabende
    * Seiten pro Veranstaltung
  * Events
    * Seiten pro Veranstaltung
* Menüspalte 4
  * E-Mail
  * Chat
  * Wiki
* Menüspalte 5
  * Pentaradio
    * Seiten pro Folge
  * Pentacast
    * Seiten pro Folge
  * Pentamusic
    * Seiten pro Folge
* Menüspalte 6
  * Shop
  * Warez

## Hierarchie neu (Entwurf)

* Startseite, darauf zwei Spalten Content:
  * Spalte 1:
    * Kurzbeschreibung, maximal drei Sätze
    * kommende Veranstaltungen
  * Spalte 2:
    * Wiki-Updates
    * News und vergangene Veranstaltungen
* Über uns
  * Anfahrt
  * Kontakt (= E-Mail + Chat)
  * Chaos macht Schule
  * Mitgliedschaft
  * Unterstützen
* Veranstaltungen (+ Seiten pro Veranstaltung)
  * Kalender
  * Themenabende
  * Events
* Podcasts (+ Seiten pro Folge)
  * Pentaradio
  * Pentacast
  * Pentamusic
  * Abonnieren (vorher: "Warez")
* Mehr Inhalte
  * Wiki
  * Artwork (vorher: "Shop")

Anmerkungen:

- Jeder Anstrich (außer die unter "Startseite") ist eine eigene Seite.
- Die Seiten "Veranstaltungen" und "Podcasts" sind jeweils eine Vereinigung der Items aus allen Unterseiten.
- Die Links auf die News-Feeds wandern von "Warez" in die entsprechende Sektion der Startseite.
